/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {AppRegistry,StyleSheet,Text,View} from 'react-native';

import {Player} from './components/Player';
import {MenuButton} from './components/MenuButton';
import {Orientation} from 'react-native-orientation';
import {Firebase} from 'firebase';


export default class EDHreactnative extends Component {
	componentDidMount(){
		firebase.initializeApp(firebaseConfig);
		Orientation.lockToLandscape();
	}

	componentWillMount(){
		
	}

	render() {
		return (
			<View style={styles.container}>
  				<View style={styles.playerRow}>
  					<Player style = {styles.player} backgroundColor = {'#E6F3FF'} cmdrPanelCol = {'#CCE6FF'} rotate = {'180 deg' }pushAway = {true}/>
  					<Player style = {styles.player} backgroundColor = {'#E6FFE6'} cmdrPanelCol = {'#CCFFCC'} rotate = {'0 deg'} pushAway = {false}/>
  				</View>
  				<View style={styles.playerRow}>
  					<Player style = {styles.player} backgroundColor = {'#FFE6FF'} cmdrPanelCol = {'#FFCCFF'} rotate = {'180 deg'} pushAway = {false}/>
  					<Player style = {styles.player} backgroundColor = {'#FFFFE6'} cmdrPanelCol = {'#FFFFCC'} rotate = {'0 deg'} pushAway = {true}/>
  				</View>
  				<MenuButton style = {styles.menuButton}/>
  			</View>
		);
	}
}

const styles = StyleSheet.create({
	player :{
		flex : 1
	},

	playerRow: {
		flex: 1,
	},

	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	menuButton:{
		
		position: 'absolute'
	},
	
});

const firebaseConfig = {
	apiKey: "my_key",
    	authDomain: "domain",
    	databaseURL: "database",
    	projectId: "edh-life-tracker",
    	storageBucket: "bucket",
    	messagingSender: "12424124124"
};

AppRegistry.registerComponent('EDHreactnative', () => EDHreactnative);
