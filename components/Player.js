import React, { Component } from 'react';
import {Button,Animated,Image,Text,View,StyleSheet} from 'react-native';
import {LifeButton}  from './LifeButton';

export class Player extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			initialLife : 40,
			CmdrPanelPos: new Animated.Value(0),
		};
	};

	incrementLife(value){
		this.setState({
			initialLife: this.state.initialLife + value
		});
	};

	decrementLife(value){
		this.setState({
			initialLife: this.state.initialLife - value
		});
	};
	
	slideOutCmdrPanel(){
		Animated.timing( this.state.CmdrPanelPos,{
				toValue: 300,
				duration: 1000,
			}).start();
	};
	
	render() {
		if(this.props.pushAway){
			return (
			<View style = {[styles.container, { backgroundColor: this.props.backgroundColor }, {transform:[{rotate: this.props.rotate}]} ]}>
				<View style = {styles.portrait}>
					<Button title = {'push'} onPress = {this.slideOutCmdrPanel.bind(this)}></Button>
				</View>
				<View style = {styles.lifeContainer}>
					<View style = {styles.playerLife}>
						<View style = {styles.lifeButtonRow}>
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"1"} incrementFunc = {(value) => this.incrementLife(1)} />
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"5"} incrementFunc = {(value) => this.incrementLife(5)} />
						</View>
						<View style = {styles.lifeButtonRow}>
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"-1"} incrementFunc = {(value) => this.decrementLife(1)} />
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"-5"} incrementFunc = {(value) => this.decrementLife(5)} />
						</View>		
						<Text style = {styles.lifeText}>{this.state.initialLife}</Text>
					</View>
					<Animated.View style = {[styles.cmdrPanel,{transform: [{translateX: this.state.CmdrPanelPos}]},{ backgroundColor: this.props.cmdrPanelCol }]}>
						<Image style = {[styles.cmd_panel, {transform:[{rotate: this.props.rotate}]}]} source={require('EDHreactnative/assets/images/cmd_panel.png')}/>
					</Animated.View>
				</View>
			</View>
		);
		}else{
			return (
			<View style = {[styles.container,  {transform:[{rotate: this.props.rotate}]} ]}>
				<View style = {styles.lifeContainer}>
					<View style = {styles.playerLife}>
						<View style = {styles.lifeButtonRow}>
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"1"} incrementFunc = {(value) => this.incrementLife(1)} />
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"5"} incrementFunc = {(value) => this.incrementLife(5)} />
						</View>
						<View style = {styles.lifeButtonRow}>
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"-1"} incrementFunc = {(value) => this.decrementLife(1)} />
							<LifeButton  bcCol = {this.props.backgroundColor} incrementVal = {"-5"} incrementFunc = {(value) => this.decrementLife(5)} />
						</View>		
						<Text style = {styles.lifeText}>{this.state.initialLife}</Text>
					</View>
					<Animated.View style = {[styles.cmdrPanel,{transform: [{translateX: this.state.CmdrPanelPos}]} ,{ backgroundColor: this.props.cmdrPanelCol }]}>
						<Image style = {[styles.cmd_panel, {transform:[{rotate: this.props.rotate}]}]} source={require('EDHreactnative/assets/images/cmd_panel.png')}/>
					</Animated.View>
					
				</View>
				<View style = {styles.portrait}>
					<Button title = {'push'} onPress = {this.slideOutCmdrPanel.bind(this)}></Button>
				</View>
			</View>
		);
		}

		
	}
}



const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		borderWidth: 1,
		borderColor: '#FFFFFF',
	},

	playerLife:{
		flex: 1,
		backgroundColor: '#F2F2F2',
		zIndex: 1,
		justifyContent: 'center',
		alignItems: 'center',

	},
	lifeContainer:{
		flex: 1.5,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	portrait:{
		flex: 1,
		backgroundColor : '#123456'

	},
	lifeButtonRow: {
		flex:1,
		flexDirection: 'row',
		backgroundColor: 'rgba(0,0,0,0)',
	},
	lifeButton:{
		borderWidth: 1,
		borderColor: '#FFFFFF',
	},
	lifeText:{
		flex:1,
		position: 'absolute',
		fontSize: 30,
   		backgroundColor: 'rgba(0,0,0,0)',
	},
	buttonBack:{
		flex :1,
		resizeMode: 'contain',
	},
	cmdrPanel:{
		zIndex: 0,
		position:'absolute',
		flexDirection:'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	cmd_panel:{
		flex: 1,
		resizeMode: 'contain',
	},


});