import React, { Component } from 'react';
import {Image,View, Button, TouchableOpacity} from 'react-native';


export class MenuButton extends React.Component{
	constructor(props){
		super(props)
	}

	render(){
		return(
			<TouchableOpacity style = {styles.menuButton}>
				<Image style = {styles.image} source={require('EDHreactnative/assets/images/menu_btn.png')}/>
			</TouchableOpacity>
		);
	}
}

const styles={
	menuButton:{
		flex : 1,
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
	},

	image:{
		resizeMode: 'contain',
		height: 65,
		width: 65,
	}
};