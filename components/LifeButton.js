import React, { Component } from 'react';
import {Text,View, Button, TouchableOpacity} from 'react-native';


export class LifeButton extends React.Component{
	constructor(props){
		super(props)
	}

	render(){
		return(
			<TouchableOpacity style = {[styles.lifeButton, { backgroundColor: this.props.bcCol }]} onPress = {this.props.incrementFunc}>
				<Text style = {styles.buttonText}>
					{this.props.incrementVal}
				</Text>
			</TouchableOpacity>
		);
	}
}

const styles={
	lifeButton:{
		flex : 1,
		alignSelf: "stretch",
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1,
		borderColor: '#FFFFFF',
	},
	buttonText:{

	}
};

/*
LifeButton.propTypes = {
	incrementVal : React.propTypes.number,
	incrementFunc: React.propTypes.func
};
*/